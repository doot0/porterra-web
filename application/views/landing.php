<!DOCTYPE HTML>
<html lang="en-GB">
<head>
    <meta charset="UTF-8">
    <title>Project Woodbeat :: Welcome</title>
    <script src="/js/bootloader.js"></script>
    <script>
        head.js(
            "/js/ext/modernizr.js",
            "//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js",
            "/js/porterra/utils.js"
        )
    </script>
    <link rel="stylesheet" type="text/css" href="/css/skeleton.css" media="all">
    <link rel="stylesheet" type="text/css" href="/css/application.css" media="all">
</head>
<body>
    <div class="container">
        <div class="sixteen columns">
            <h1>Your music means the world to you &mdash; so make it portable.</h1>
            <hr>
            <p>Welcome to Project Woodbeat.</p>
            <p>We're not quite done with coding up the greatest music service that's yet to come, but we'll be here soon. Promise.</p>
            <p>In the mean time... </p>

            <?php echo form_open('signup');?>
            <div class="form-title">Keep me updated</div>
            <?php echo form_label('Email', 'email')?>
            <?php $inputAttributes = array('id' => 'email', 'name' => 'email', 'type' => "text")?>

            <?php echo form_input($inputAttributes)?>
            <?php $submitAttributes = array('value' => 'Sign me up', 'name' => 'submit', 'type' => 'button')?>

            <?php echo form_submit($submitAttributes)?>
            <div class="feedback">
                <?php echo validation_errors();?>
            </div>
            <?php echo form_close()?>

        </div>
    </div>
</body>
<script>window.jQuery || document.write('<script src="/js/ext/jquery-1.8.2.min.js"><\/script>')</script>
<script>
    $(function() {

        $(window).keydown(function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
        });

        $('input[type="button"]').click(function() {

            var Forms = Porterra.Utils.Forms,
                validateEmail = Porterra.Utils.Validate.isEmail,
                email = $('input[type="text"]'),
                submit = $('input[type="button"]'),
                feedback = $('form .feedback');

            email.on('focus', function(){
                Forms.enableInput($(this));
                submit.attr('value', 'Sign me up');
            });

            if (validateEmail(email.val())) {
                $(this).attr('value', 'Wait up...');
                Forms.disableInput(email);
                Forms.disableInput(submit);

                feedback.hide();

                var time = Porterra.Utils.Client.getUserTime();

                var address = email.val(),
                    mailObj = {'email': address, 'time': time};

                $.post('signup', mailObj, function(response) {
                    $('form .form-title').animate({opacity:.5}, 200);
                    $('label').animate({opacity: .1}, 200);
                    email.animate({opacity: .1}, 200);
                    submit.animate({opacity: .1}, 200);

                    feedback.animate({height: '100%','margin-top': '20px'}, 200);
                    feedback.text(response).fadeIn(500);

                });

            } else {
                $(this).attr('value', 'That\'s not right!')
            }

        });
    })
</script>

</html>