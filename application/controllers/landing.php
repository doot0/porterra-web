<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

    public function index()
    {
        $this->load->helper('form');

        $this->load->view('landing');
    }

}