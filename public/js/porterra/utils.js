'use strict';
var Porterra = {};

Porterra.Utils = {

    Validate : {
        isEmail : function (email) {
            var regex = /\S+@\S+\.\S+/;
            return regex.test(email);
        }
    },

    Forms : {

        disableInput : function (targets) {
            targets = $(targets).filter('input');
            targets.attr('disabled', 'disabled');
        },

        enableInput : function (targets) {
            targets = $(targets).filter('input');
            targets.removeAttr('disabled');
        }
    },

    Client : {

        getUserTime : function () {
            var t = new Date();
            return t.getHours() + ':' + t.getMinutes() + ':' + t.getSeconds();
        }

    },

    Debug : {

        init : function () {
            $('body').detach('.debug-element')
                     .prepend('<div class="debug-element"><div class="width">W:</div><div class="height">H:</div></div>');
        },

        winDimensions : function () {
            this.init();
            $(window).resize(function () {
                $('.debug-element .width').text('W:' + $(window).width());
                $('.debug-element .height').text('H:' + $(window).height());
            });
        },

        destroy : function () {
            $('.debug-element').detach()
                               .remove();
        }

    }

};