var Porterra = new function(){
	
	this.setEvents = function(){
			//Any events we need to set go here
			
			console.log( "EVENTS SET" );
		}
		
	//All of the following code can be accessed by the JS Object Porterra.player
	this.Player = new function(){
		this.play = function(){
		audio.play();
			return "SUCCESS";
		}
		this.pause = function(){
			audio.pause();
			return "SUCCESS";
		}
		this.volUp = function(){
			var currentVolume = audio.volume;
			audio.volume = currentVolume + 0.1;
		}
		this.volDown = function(){
			var currentVolume = audio.volume;
			audio.volume = currentVolume - 0.1;
		}
	this.Player.Network = new function(){
		this.getNetworkState = function(){
			var ns = audio.networkState;
			var state = "";
				switch(ns){
					case 0:
						state = "NETWORK_EMPTY"; //NETWORK IS NOT TRANSFERRING DATA
					break;
					
					case 1:
						state = "NETWORK_IDLE"; //NETWORK HAS FINISHED TRANSFERRING DATA
					break;
					
					case 2:
						state = "NETWORK_LOADING"; //NETWORK IS CURRENTLY TRANSFERRING DATA
					break;
					
					case 3:
						state = "NETWORK_NO_SOURCE"; //THE AUDIO SOURCE IS NOT VALID
					break;
				}
			return state;
		}
	}
}
	this.init = function(){
	console.log("Porterra is initialising...");
	var audio = document.getElementById('porterra-player');
	var networkStateIndicator = document.getElementById('network-state');
	var currentTrackDownloadProgress = 0;
	var defaultVolume = 0.8;
	this.type = "player";
	this.version = "0.0.1";
	Porterra.setEvents();
	audio.volume = defaultVolume;
	return "SUCCESS";
	}
}