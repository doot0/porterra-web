#DEVELOPMENT ON PORTERRA HAS CEASED INDEFINITELY

# Porterra

Versioning is incremented according to this document: [http://semver.org/](http://semver.org).

## Setting up Porterra on XAMPP
Drop the archive you download into a directory you specify which XAMPP can access. Here is an example VirtualHost configuration for Porterra which I use;

    <VirtualHost *:80>
        ServerName porterra.dev
	    DocumentRoot W:\xampp\htdocs\porterra\public
	    <Directory W:\xampp\htdocs\porterra\public>
	        Options All
	        AllowOverride All
	        Order allow,deny
	        Allow from all
	    </Directory>
    </VirtualHost>

Swap out the `DocumentRoot` and `<Directory>` directives as per your needs.

Point `porterra.dev` to 127.0.0.1 in your hosts file :: `%windir%/System32/drivers/etc/hosts`. Restart the Apache service in XAMPP.

Navigate to `/application/public`. Create a `.htaccess` file and put this in it;

    RewriteEngine On
    RewriteCond %{REQUEST_FILENAME} -s [OR]
    RewriteCond %{REQUEST_FILENAME} -l
    RewriteRule ^.*$ - [NC,L]
    RewriteRule ^.*$ index.php [NC,L]

Now check out `http://porterra.dev`. You're done.
